package kz.iitu.recipeapp.recipeapp.controller;

import java.util.List;
import java.util.Optional;

import kz.iitu.recipeapp.recipeapp.model.Category;
import kz.iitu.recipeapp.recipeapp.model.Ingredients;
import kz.iitu.recipeapp.recipeapp.repository.CategoryRepository;
import kz.iitu.recipeapp.recipeapp.repository.IngredientsRepository;
import kz.iitu.recipeapp.recipeapp.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kz.iitu.recipeapp.recipeapp.model.Recipe;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class RecipeController {
 
  @Autowired
  RecipeRepository recipeRepository;
  
  @Autowired
  IngredientsRepository ingredientsRepository;

  @Autowired
  CategoryRepository categoryRepository;
  
  //return all ingredients
  @GetMapping(value="/ingredients")
  public List<Ingredients> getIngrendientsOrderByName() {
    return (List<Ingredients>) ingredientsRepository.findAllByOrderByName();
  }

  //return all categories
  @GetMapping(value="/category")
  public List<Category> getCategoriesByName() {
    return (List<Category>) categoryRepository.findAllByOrderByName();
  }
  
  //return ingredients for recipe
  @GetMapping("/ingredients/recipes/{id}")
  public List<Recipe> getIngredientsByRecipeId(@PathVariable(value = "id") long id){
	  return (List<Recipe>) recipeRepository.findIngredientsById(id);
  }	
 
  //Retorna todas as receitas
  @GetMapping("/recipes")
  public List<Recipe> getAllRecipes() {
    return (List<Recipe>) recipeRepository.findAll();
  }
 
  //Retorna receita por id
  @GetMapping("/recipes/{id}")
  public Optional<Recipe> getRecipeById(@PathVariable(value = "id") long id){
	return recipeRepository.findById(id);
  }
  
  //Retorna receitas que contenham id de ingrediente
  @GetMapping("/recipes/ingredients/{id}")
  public List<Recipe> getRecipeByIngredientsId(@PathVariable(value = "id") long id){
	return recipeRepository.findByIngredientsId(id);
  }

  @GetMapping("/recipes/categories/{id}")
  public List<Recipe> getRecipeByCategoriesId(@PathVariable(value = "id") long id){
    return recipeRepository.findByCategoriesId(id);
  }
  

  @PostMapping(value = "/recipes/create")
  public Recipe postRecipe(@RequestBody Recipe recipe) {
    Recipe _recipe = recipeRepository.save(new Recipe(recipe.getName(), recipe.getServings(), recipe.getCalories(), recipe.getIngredients(), recipe.getHowTo(), recipe.getCategories()));
    return _recipe;
  }
 
  //delete recipe by id
  @DeleteMapping("/recipes/{id}")
  public ResponseEntity<String> deleteRecipe(@PathVariable("id") long id) {
    recipeRepository.deleteById(id);
    return new ResponseEntity<>("Recipe has been deleted!", HttpStatus.OK);
  }
 
  //delete all recepies
  @DeleteMapping("/recipes/delete")
  public ResponseEntity<String> deleteAllRecipes() {
    recipeRepository.deleteAll();
    return new ResponseEntity<>("All recipes have been deleted!", HttpStatus.OK);
  }
  //update recipe by id
  @PutMapping("/recipes/{id}")
  public ResponseEntity<Recipe> updateRecipe(@PathVariable("id") long id, @RequestBody Recipe recipe) {
    Optional<Recipe> recipeData = recipeRepository.findById(id);
    
    if (recipeData.isPresent()) {
      Recipe _recipe = recipeData.get();
      _recipe.setName(recipe.getName());
      _recipe.setServings(recipe.getServings());
      _recipe.setCalories(recipe.getCalories());
      _recipe.setIngredients(recipe.getIngredients());
      _recipe.setHowTo(recipe.getHowTo());
      _recipe.setCategories(recipe.getCategories());
      return new ResponseEntity<>(recipeRepository.save(_recipe), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
