package kz.iitu.recipeapp.recipeapp.model.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class TokenResponse {

    private String accessToken;
    private String refreshToken;
    public TokenResponse(String accessToken, String refreshToken){
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public TokenResponse(String accessToken, String refreshToken, List<String> listRoles){
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
