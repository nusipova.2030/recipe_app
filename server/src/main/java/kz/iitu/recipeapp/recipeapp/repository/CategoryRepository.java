package kz.iitu.recipeapp.recipeapp.repository;

import kz.iitu.recipeapp.recipeapp.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
public interface CategoryRepository extends CrudRepository<Category, Long> {

    List<Category> findAllByOrderByName();
}
