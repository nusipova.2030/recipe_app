package kz.iitu.recipeapp.recipeapp.repository;

import kz.iitu.recipeapp.recipeapp.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
public interface RoleRepository extends CrudRepository<Role, Long> {

}
