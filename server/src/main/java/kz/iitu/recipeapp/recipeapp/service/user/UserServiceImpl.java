package kz.iitu.recipeapp.recipeapp.service.user;
import io.jsonwebtoken.impl.DefaultClaims;
import kz.iitu.recipeapp.recipeapp.constants.ClaimKeysConstants;
import kz.iitu.recipeapp.recipeapp.exceptions.BadRequestException;
import kz.iitu.recipeapp.recipeapp.exceptions.CustomAuthenticationException;
import kz.iitu.recipeapp.recipeapp.exceptions.CustomBadRequestException;
import kz.iitu.recipeapp.recipeapp.exceptions.CustomNotFoundException;
import kz.iitu.recipeapp.recipeapp.model.User;
import kz.iitu.recipeapp.recipeapp.model.dto.request.LoginRequest;
import kz.iitu.recipeapp.recipeapp.model.dto.request.RegisterRequest;
import kz.iitu.recipeapp.recipeapp.model.dto.response.TokenResponse;
import kz.iitu.recipeapp.recipeapp.repository.UserRepository;
import kz.iitu.recipeapp.recipeapp.service.token.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final TokenService tokenService;

    @Autowired
    public UserServiceImpl(UserRepository repository, BCryptPasswordEncoder bCryptPasswordEncoder, TokenService tokenService) {
        this.repository = repository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.tokenService = tokenService;
    }

    @Override
    public User findById(String id) {
        return repository.findById(UUID.fromString(id)).orElseThrow(() -> new CustomNotFoundException(String.format("User with id: %s not found", id)));
    }

    @Override
    public User findByEmail(String email) {
        return repository.findByEmail(email).orElseThrow(() -> new CustomNotFoundException(String.format("User with email: %s not found", email)));
    }

    @Override
    public ResponseEntity<?> login(LoginRequest loginRequest) {
        User user = findByEmail(loginRequest.getEmail());
        if (!bCryptPasswordEncoder.matches(loginRequest.getPassword(), user.getPassword()))
            throw new CustomBadRequestException("Bad credentials");
        if (user.isBlocked()) {
            throw new CustomAuthenticationException("User is blocked");
        }
        if (!user.isActive()) {
            throw new CustomAuthenticationException("User is not active");
        }
        Map<String, Object> claims = new HashMap<>();
        claims.put(ClaimKeysConstants.USERNAME, user.getEmail());
        List<String> roles = new ArrayList<>();
        user.getRoles().forEach(x -> roles.add(x.getName()));
        claims.put(ClaimKeysConstants.ROLES, roles);
        TokenResponse tokensResponse = tokenService.generateTokensResponse(claims);

        return new ResponseEntity<>(tokensResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> register(RegisterRequest registerRequest) {
        boolean userInDb = repository.existsByEmail(registerRequest.getEmail());
        if (userInDb) {
            return new ResponseEntity<>("User already exists", HttpStatus.CONFLICT);
        }
        if (!registerRequest.getPassword().equals(registerRequest.getRepeatedPassword())) {
            return new ResponseEntity<>("Check passwords", HttpStatus.BAD_REQUEST);
        }
        registerRequest.setPassword(bCryptPasswordEncoder.encode(registerRequest.getPassword()));
        User user = new User(registerRequest);
        repository.save(user);
        return new ResponseEntity<>("Registered", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TokenResponse> refreshToken(String token) {
        if (!tokenService.tokenValidation(token))
            throw new BadRequestException("It's not valid refresh token");

        DefaultClaims claims = tokenService.getClaimsFromToken(token);
        Map<String, Object> newClaims = new HashMap<>();
        if (!claims.get(ClaimKeysConstants.IS_REFRESH_TOKEN, Boolean.class))
            throw new BadRequestException("It's not valid refresh token");

        String email = claims.get(ClaimKeysConstants.USERNAME, String.class);

        TokenResponse tokensResponse;

        User userEmail = findByEmail(email);
        List<String> roles = new ArrayList<>();
        userEmail.getRoles().stream().forEach(x -> roles.add(x.getName()));
        newClaims.put(ClaimKeysConstants.USERNAME, userEmail.getEmail());
        tokensResponse = tokenService.generateTokensResponse(claims);

        // tokensResponse.setType(getAccountType(user.getRoles()));
        return new ResponseEntity<>(tokensResponse, HttpStatus.OK);
    }
}
