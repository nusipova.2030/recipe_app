package kz.iitu.recipeapp.recipeapp.service.userDetails;

import kz.iitu.recipeapp.recipeapp.model.User;
import kz.iitu.recipeapp.recipeapp.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.debug(String.format("IN method loadUserByUsername %s", email));
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(MessageFormat.format("User with email = {0} not found", email)));
//        if(!user.isActive()){
//            throw new CustomAuthenticationException("User is not active");
//        }
//        if (user.isBlocked()){
//            throw new CustomAuthenticationException("User is blocked");
//        }
        return new UserDetailsImpl(user);
    }
}
